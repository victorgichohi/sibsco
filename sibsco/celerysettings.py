import os
from celery import Celery

from sibsco import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sibsco.settings')

app = Celery('sibsco')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


