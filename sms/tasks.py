from celery import shared_task

from sms.handlers import Nexmo, AfricasTalking
# Sms task for all handlers
@shared_task
def send_sms_async(phone_number, message):
    handlers = [
        Nexmo(),
    ]
    for handler in handlers:
        handler.send(phonenumber=phone_number, message=message)
