# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from sms.models import Sms


class SmsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Sms._meta.get_fields()]


admin.site.register(Sms, SmsAdmin)
