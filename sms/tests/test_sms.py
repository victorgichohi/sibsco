from django.test import TestCase
from mock import patch
from sms.tasks import send_sms_async


class SMSTaskTests(TestCase):
    def test_nexmo_sms(self):
        with patch('sms.handlers.Nexmo.send') as nexmo_mock:
            send_sms_async(phone_number="254707534022", message="Testing Nexmo")
            nexmo_mock.assert_called_with(phone_number="254707534022", message="Testing Nexmo")

    def test_africas_talking_sms(self):
        with patch('sms.handlers.AfricasTalking.send') as at_mock:
            send_sms_async(phone_number="254707534022", message="Testing Africas Talking")
            at_mock.assert_called_with(phonenumber="254707534022", message="Testing Africas Talking")



