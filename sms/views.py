from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import FormView

from sms.forms import MessageForm
from sms.tasks import send_sms_async


# View for getting message and phone number and sending to send_sms_async
class MessageInputView(FormView):
    form_class = MessageForm
    template_name = 'phone.html'

    def post(self, request, *args, **kwargs):
        return super(MessageInputView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        phone_number = form.cleaned_data['phone_number']
        message = form.cleaned_data['message']
        send_sms_async.delay(phone_number, message)
        messages.success(self.request,
                         'Your message "{}" has been sent to phone number "{}"'.format(message, phone_number))
        return redirect(reverse('sms.message_input'))
