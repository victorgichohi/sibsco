from django import forms


class MessageForm(forms.Form):
    phone_number = forms.CharField(max_length=30, required=True, widget=forms.TextInput(attrs={'placeholder': 'phone number'}))
    message = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'placeholder': 'message'}))

    def clean_token(self):
        phone_number = self.cleaned_data.get('phone_number')
        try:
            phone_number = int(phone_number)
        except ValueError:
            raise forms.ValidationError('Please ensure that your phone number contains numbers only')
        return phone_number
