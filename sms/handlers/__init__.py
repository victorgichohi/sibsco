import json
from urllib import urlopen
from urllib2 import Request

import nexmo
from django.conf import settings
from django.utils.http import urlencode


class Nexmo:

    def __init__(self):
        pass

    def send(self, phonenumber, message):
        client = nexmo.Client(key=settings.NEXMO_API_KEY, secret=settings.NEXMO_API_SECRET)

        message_params = {
            'from': settings.NEXMO_DEFAULT_FROM,
            'to': str(phonenumber),
            'text': message.encode('utf-8'),
        }

        response = client.send_message(message_params)
        from sms.models import Sms
        Sms.objects.create(phone_number=phonenumber, message=message,
                           channel=Sms.Channels.Nexmo, response_from_network=response)


# This is not used since only have credentials for sandbox
class AfricasTalking:

    def __init__(self):
        pass

    def send(self, phonenumber, message):
        data = {
            'username': settings.AFRICASTALKING_USERNAME,
            'to': str(phonenumber),
            'message': message,
            'bulkSMSMode': 1,
            'from': 'Victor'
        }
        url = 'https://api.africastalking.com/version1/messaging'
        try:
            if data is not None:
                data = urlencode(data)
                request = Request(url, data)
            else:
                request = Request(url)

            request.add_header('Accept', 'application/json')
            request.add_header('apikey', settings.AFRICASTALKING_API_KEY)
            response = urlopen(request)
        except Exception as e:
            raise Exception(str(e))
        else:
            response_data = json.loads(response.read())
            from sms.models import Sms
            Sms.objects.create(phone_number=phonenumber, message=message,
                               channel=Sms.Channels.AfricasTalking, response_from_network=response_data)
