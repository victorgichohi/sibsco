from django.db import models
from djchoices import DjangoChoices, ChoiceItem


class Sms(models.Model):
    # Add different channels here
    class Channels(DjangoChoices):
        Nexmo = ChoiceItem('nexmo', 'Nexmo')
        AfricasTalking = ChoiceItem('africastalking', 'AfricasTalking')

    phone_number = models.CharField(max_length=50, null=True, blank=True)
    message = models.TextField(blank=True, null=True)
    channel = models.CharField(max_length=50, choices=Channels.choices, null=True, blank=True)
    response_from_network = models.TextField(blank=True, null=True)
